package com.example.BookReservation.service;

import com.example.BookReservation.entity.Aluno;
import com.example.BookReservation.entity.Livro;
import com.example.BookReservation.entity.Reserva;
import com.example.BookReservation.repository.AlunoRepository;
import com.example.BookReservation.repository.LivroRepository;
import com.example.BookReservation.repository.ReservaRepository;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ReservaService {

    private final ReservaRepository reservaRepository;
    private final LivroRepository livroRepository;
    private final AlunoRepository alunoRepository;

    public ReservaService(ReservaRepository reservaRepository, LivroRepository livroRepository, AlunoRepository alunoRepository) {
        this.reservaRepository = reservaRepository;
        this.livroRepository = livroRepository;
        this.alunoRepository = alunoRepository;
    }

    public Reserva create(Reserva reserva) {
        Livro livro = livroRepository.findById(reserva.getLivro().getId())
                .orElseThrow(() -> new RuntimeException("Livro não encontrado com ID " + reserva.getLivro().getId()));

        Aluno aluno = alunoRepository.findById(reserva.getIdAluno())
                .orElseThrow(() -> new RuntimeException("Aluno não encontrado com ID " + reserva.getIdAluno()));

        int copiasDisponiveis = livro.getCopiasDisponiveis();

        if (copiasDisponiveis > 0) {
            livro.setCopiasDisponiveis(copiasDisponiveis - 1);
            livroRepository.save(livro);
            reserva.setLivro(livro); // Associando o livro encontrado à reserva
            return reservaRepository.save(reserva);
        } else {
            throw new RuntimeException("Não há cópias disponíveis para o livro com ID " + livro.getId());
        }
    }

    public List<Reserva> findByIdAluno(Long idAluno) {
        return reservaRepository.findByIdAluno(idAluno);
    }
}