package com.example.BookReservation.controller;

import com.example.BookReservation.entity.DTO.LivroDTO;
import com.example.BookReservation.entity.Livro;
import com.example.BookReservation.service.LivroService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/livros")
public class LivroController {

    private final LivroService livroService;

    public LivroController(LivroService livroService) {
        this.livroService = livroService;
    }

    @GetMapping
    public ResponseEntity<List<LivroDTO>> findAll() {
        List<Livro> livros = livroService.findAll();
        List<LivroDTO> livroDTOs = livros.stream()
                .map(LivroDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(livroDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<LivroDTO> findById(@PathVariable Long id) {
        Livro livro = livroService.findById(id);
        return ResponseEntity.ok().body(new LivroDTO(livro));
    }

    @GetMapping("/titulo/{titulo}")
    public ResponseEntity<List<LivroDTO>> findByTitulo(@PathVariable String titulo) {
        List<Livro> livros = livroService.findByTitulo(titulo);
        List<LivroDTO> livroDTOs = livros.stream()
                .map(LivroDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(livroDTOs);
    }

    @GetMapping("/autor/{autor}")
    public ResponseEntity<List<LivroDTO>> findByAutor(@PathVariable String autor) {
        List<Livro> livros = livroService.findByAutor(autor);
        List<LivroDTO> livroDTOs = livros.stream()
                .map(LivroDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(livroDTOs);
    }

    @GetMapping("/categoria/{categoria}")
    public ResponseEntity<List<LivroDTO>> findByCategoria(@PathVariable String categoria) {
        List<Livro> livros = livroService.findByCategoria(categoria);
        List<LivroDTO> livroDTOs = livros.stream()
                .map(LivroDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(livroDTOs);
    }

    @PostMapping
    public ResponseEntity<LivroDTO> create(@RequestBody LivroDTO livroDTO) {
        Livro livro = livroService.create(livroDTO.toLivro());
        return ResponseEntity.status(HttpStatus.CREATED).body(new LivroDTO(livro));
    }
}