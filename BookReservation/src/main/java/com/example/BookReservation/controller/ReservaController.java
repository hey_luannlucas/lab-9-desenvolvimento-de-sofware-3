package com.example.BookReservation.controller;

import com.example.BookReservation.entity.DTO.ReservaDTO;
import com.example.BookReservation.entity.Reserva;
import com.example.BookReservation.service.ReservaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/reservas")
public class ReservaController {

    private final ReservaService reservaService;

    public ReservaController(ReservaService reservaService) {
        this.reservaService = reservaService;
    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestBody ReservaDTO reservaDTO) {
        try {
            Reserva novaReserva = reservaService.create(reservaDTO.toReserva());
            ReservaDTO reservaCriada = new ReservaDTO(novaReserva);
            return ResponseEntity.status(HttpStatus.CREATED).body(reservaCriada);
        } catch (RuntimeException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/{idAluno}")
    public ResponseEntity<Object> findByIdAluno(@PathVariable Long idAluno) {
        List<Reserva> reservas = reservaService.findByIdAluno(idAluno);
        List<ReservaDTO> reservaDTOs = reservas.stream()
                .map(ReservaDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(reservaDTOs);
    }
}
