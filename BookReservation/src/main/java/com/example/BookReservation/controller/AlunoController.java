package com.example.BookReservation.controller;

import com.example.BookReservation.entity.Aluno;
import com.example.BookReservation.entity.DTO.AlunoDTO;
import com.example.BookReservation.service.AlunoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/alunos")
public class AlunoController {

    private final AlunoService alunoService;

    public AlunoController(AlunoService alunoService) {
        this.alunoService = alunoService;
    }

    @GetMapping
    public ResponseEntity<List<AlunoDTO>> findAll() {
        List<Aluno> alunos = alunoService.findAll();
        List<AlunoDTO> alunoDTOs = alunos.stream()
                .map(AlunoDTO::new)
                .collect(Collectors.toList());
        return ResponseEntity.ok().body(alunoDTOs);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AlunoDTO> findById(@PathVariable Long id) {
        Aluno aluno = alunoService.findById(id);
        return ResponseEntity.ok().body(new AlunoDTO(aluno));
    }

    @PostMapping
    public ResponseEntity<AlunoDTO> create(@RequestBody AlunoDTO alunoDTO) {
        Aluno aluno = alunoService.create(alunoDTO.toAluno());
        return ResponseEntity.status(HttpStatus.CREATED).body(new AlunoDTO(aluno));
    }
}