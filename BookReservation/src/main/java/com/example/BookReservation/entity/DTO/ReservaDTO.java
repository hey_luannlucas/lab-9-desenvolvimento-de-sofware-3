package com.example.BookReservation.entity.DTO;

import com.example.BookReservation.entity.Reserva;
import lombok.Data;
import org.springframework.http.ResponseEntity;

@Data
public class ReservaDTO {
    private Long id;
    private Long idAluno;
    private LivroDTO livro;
    public ReservaDTO() {
    }

    public ReservaDTO(Reserva reserva) {
        this.id = reserva.getId();
        this.idAluno = reserva.getIdAluno();
        this.livro = new LivroDTO(reserva.getLivro());
    }

    public ReservaDTO(ResponseEntity<?> novaReserva) {

    }

    public Reserva toReserva() {
        Reserva reserva = new Reserva();
        reserva.setId(this.id);
        reserva.setIdAluno(this.idAluno);
        reserva.setLivro(this.livro.toLivro());
        return reserva;
    }
}
