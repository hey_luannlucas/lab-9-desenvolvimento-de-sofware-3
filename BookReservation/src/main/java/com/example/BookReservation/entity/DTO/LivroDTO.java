package com.example.BookReservation.entity.DTO;

import com.example.BookReservation.entity.Livro;
import lombok.Data;

@Data
public class LivroDTO {
    private Long id;
    private String titulo;
    private String autor;
    private String categoria;
    private int copiasDisponiveis;
    public LivroDTO() {
    }

    public LivroDTO(Livro livro) {
        this.id = livro.getId();
        this.titulo = livro.getTitulo();
        this.autor = livro.getAutor();
        this.categoria = livro.getCategoria();
        this.copiasDisponiveis = livro.getCopiasDisponiveis();
    }

    public Livro toLivro() {
        Livro livro = new Livro();
        livro.setId(this.id);
        livro.setTitulo(this.titulo);
        livro.setAutor(this.autor);
        livro.setCategoria(this.categoria);
        livro.setCopiasDisponiveis(this.copiasDisponiveis);
        return livro;
    }
}
