package com.example.BookReservation.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class Reserva {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long idAluno;

    @ManyToOne
    @JoinColumn(name = "livro_id")
    private Livro livro;

    public Long getLivroId() {
        return livro != null ? livro.getId() : null;
    }
}
